const { eventValidator } = require('../middlewares/validators/validators');
const { jwtMiddlewareByRole } = require('../middlewares/jwt/jwt.middleware');

const initEvents = (Router, services) => {
  const { OddsService: oddsService, EventService: eventService } = services;
  const router = Router();

  router.post('/',
    eventValidator.PostEventValidator,
    jwtMiddlewareByRole,
    (req, res, next) => {
      req.body.odds.home_win = req.body.odds.homeWin;
      delete req.body.odds.homeWin;
      req.body.odds.away_win = req.body.odds.awayWin;
      delete req.body.odds.awayWin;

      oddsService
        .create(req.body.odds)
        .then((odds) => {
          delete req.body.odds;
          req.body.away_team = req.body.awayTeam;
          req.body.home_team = req.body.homeTeam;
          req.body.start_at = req.body.startAt;
          delete req.body.awayTeam;
          delete req.body.homeTeam;
          delete req.body.startAt;
          
          eventService
            .create({
              ...req.body,
              odds_id: odds.id,
            })
            .then((event) => {
              [
                'bet_amount',
                'event_id',
                'away_team',
                'home_team',
                'odds_id',
                'start_at',
              ].forEach((key) => {
                let index = key.indexOf('_');
                let newKey = key.replace('_', '');
                newKey = newKey.split('');
                newKey[index] = newKey[index].toUpperCase();
                newKey = newKey.join('');
                event[newKey] = event[key];
                delete event[key];
              });

              ['home_win', 'away_win'].forEach((key) => {
                let index = key.indexOf('_');
                let newKey = key.replace('_', '');
                newKey = newKey.split('');
                newKey[index] = newKey[index].toUpperCase();
                newKey = newKey.join('');
                odds[newKey] = odds[key];
                delete odds[key];
              });

              return res.send({
                ...event,
                odds,
              });
            })
            .catch(next);
        })
        .catch(next);
    }
  );

  return router;
};

module.exports = { initEvents };
