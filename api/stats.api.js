const { jwtMiddlewareByRole } = require('../middlewares/jwt/jwt.middleware');

const initUser = (Router, services) => {
  const { } = services;
  const router = Router();

  router.get('/', jwtMiddlewareByRole, (req, res, next) => { });
  return router;
};

module.exports = { initUser };
