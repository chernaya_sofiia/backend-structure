const {
  transactionValidator,
} = require('../middlewares/validators/validators');
const { HttpError } = require('../exceptions/exceptions');
const { jwtMiddlewareByRole } = require('../middlewares/jwt/jwt.middleware');

const initTransaction = (Router, services) => {
  const { TransactionService: transactionService, UserService: userService } =
    services;
  const router = Router();

  router.post(
    '/',
    transactionValidator.PostTransactionValidator,
    jwtMiddlewareByRole,
    (req, res, next) => {
      console.log(req.body.userId);
      userService
        .getUserById(req.body.userId)
        .then(([user]) => {
          if (!user) {
            throw new HttpError({
              status: 400,
              message: 'User does not exist',
            });
          } else {
            req.body.card_number = req.body.cardNumber;
            delete req.body.cardNumber;
            req.body.user_id = req.body.userId;
            delete req.body.userId;

            transactionService.create(req.body)
              .then((result) => {
                const currentBalance = req.body.amount + user.balance;
                userService
                  .update({
                    id: req.body.user_id,
                    balance: currentBalance,
                  })
                  .then(() => {
                    ['user_id', 'card_number'].forEach((key) => {
                      const index = key.indexOf('_');
                      let newKey = key.replace('_', '');
                      newKey = newKey.split('');
                      newKey[index] = newKey[index].toUpperCase();
                      newKey = newKey.join('');
                      result[newKey] = result[key];
                      delete result[key];
                    });
                    return res.send({
                      ...result,
                      currentBalance,
                    });
                  })
                  .catch(next);
              });
          }
        })
        .catch(next);
    }
  );

  return router;
};

module.exports = { initTransaction };
