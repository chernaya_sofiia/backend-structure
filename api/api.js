const { initUser } = require('./users.api');
const { initTransaction } = require('./transactions.api');
const { initEvents } = require('./events.api');
const {
  UserService,
  EventService,
  TransactionService,
  AuthService,
  OddsService,
} = require('../services/services');

const initApi = (Router) => {
  const apiRouter = Router();

  apiRouter.use(
    '/users', initUser(Router, {
      UserService,
      AuthService
    }));
  apiRouter.use(
    '/transactions', initTransaction(Router, {
      TransactionService,
      UserService
    })
  );
  apiRouter.use(
    '/events', initEvents(Router, {
      OddsService,
      EventService
    })
  );

  return apiRouter;
};

module.exports = { initApi };
