class BetService {
  constructor({ betRepository }) {
    this._betRepository = betRepository;
  }

  getBets(filter) {
    return this._betRepository.getAll(filter);
  }

  getBetById(id) {
    return this._betRepository.getById(id);
  }

  create(id) {
    return this._betRepository.create(id);
  }

  delete(id) {
    return this._betRepository.deleteById(id);
  }

  update(data) {
    return this._betRepository.updateById(data);
  }
}

module.exports = BetService;
