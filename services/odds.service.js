class OddsService {
  constructor({ oddsRepository }) {
    this._oddsRepository = oddsRepository;
  }

  getOdds(filter) {
    return this._oddsRepository.getAll(filter);
  }

  getOddsById(id) {
    return this._oddsRepository.getById(id);
  }

  create(id) {
    return this._oddsRepository.create(id);
  }

  delete(id) {
    return this._oddsRepository.deleteById(id);
  }

  update(data) {
    return this._oddsRepository.updateById(data);
  }
}

module.exports = OddsService;