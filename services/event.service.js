class EventService {
  constructor({ eventRepository }) {
    this._eventRepository = eventRepository;
  }

  getEvents(filter) {
    return this._eventRepository.getAll(filter);
  }

  getEventById(id) {
    return this._eventRepository.getById(id);
  }

  create(id) {
    return this._eventRepository.create(id);
  }

  delete(id) {
    return this._eventRepository.deleteById(id);
  }

  update(data) {
    return this._eventRepository.updateById(data);
  }
}

module.exports = EventService;
