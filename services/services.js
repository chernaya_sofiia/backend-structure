const AuthServiceClass = require('./auth.service');
const BetServiceClass = require('./bet.service');
const EventServiceClass = require('./event.service');
const TransactionServiceClass = require('./transaction.service');
const OddsServiceClass = require('./odds.service');
const UserServiceClass = require('./user.service');
const {
  Bet,
  User,
  Event,
  Transaction,
  Odds,
} = require('../data/repositories/repositories');

const UserService = new UserServiceClass({ userRepository: User });
const BetService = new BetServiceClass({ betRepository: Bet });
const EventService = new EventServiceClass({ eventRepository: Event });
const TransactionService = new TransactionServiceClass({
  transactionRepository: Transaction,
});
const AuthService = new AuthServiceClass();
const OddsService = new OddsServiceClass({ oddsRepository: Odds });

module.exports = {
  UserService,
  BetService,
  EventService,
  TransactionService,
  AuthService,
  OddsService,
};
