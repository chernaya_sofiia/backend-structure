class TransactionService {
  constructor({ transactionRepository }) {
    this._transactionRepository = transactionRepository;
  }

  getTransactions(filter) {
    return this._transactionRepository.getAll(filter);
  }

  getTransactionById(id) {
    return this._transactionRepository.getById(id);
  }

  create(id) {
    return this._transactionRepository.create(id);
  }

  delete(id) {
    return this._transactionRepository.deleteById(id);
  }

  update(data) {
    return this._transactionRepository.updateById(data);
  }
}

module.exports = TransactionService;
