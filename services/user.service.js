class UserService {
  constructor({ userRepository }) {
    this._userRepository = userRepository;
  }

  getUsers(filter) {
    return this._userRepository.getAll(filter);
  }

  getUserById(id) {
    return this._userRepository.getById(id);
  }

  create(id) {
    return this._userRepository.create(id);
  }

  delete(id) {
    return this._userRepository.deleteById(id);
  }

  update(data) {
    return this._userRepository.updateById(data);
  }
}

module.exports = UserService;
