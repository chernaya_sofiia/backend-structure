const { ENV } = require('../common/enums/app/env.enum');

module.exports = {
  development: {
    client: 'postgresql',
    connection: {
      port: ENV.DB.PORT,
      host: ENV.DB.HOST,
      database: ENV.DB.NAME,
      user: ENV.DB.USER,
      password: ENV.DB.PASSWORD,
    },
    migrations: {
      directory: './db/migrations',
      tableName: 'knex_migrations',
    },
    seeds: {
      directory: './db/seeds',
    },
    pool: {
      min: 0,
      max: 20,
    },
  },
};
