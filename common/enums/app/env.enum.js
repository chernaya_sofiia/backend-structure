const { config } = require('dotenv');

config();

const {
  DATABASE_PORT,
  DATABASE_HOST,
  DATABASE_NAME,
  DATABASE_USER,
  DATABASE_ACCESS_KEY,
  JWT_SECRET,
} = process.env;

const ENV = {
  APP: {
    API_PATH: '/',
    PORT: 3000,
  },
  JWT: {
    SECRET: JWT_SECRET,
  },
  DB: {
    PORT: DATABASE_PORT,
    HOST: DATABASE_HOST,
    NAME: DATABASE_NAME,
    USER: DATABASE_USER,
    PASSWORD: DATABASE_ACCESS_KEY,
  },
};

module.exports = { ENV };
