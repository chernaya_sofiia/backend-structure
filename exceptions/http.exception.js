const DEFAULT_MESSAGE = 'Internal Server Error';

class HttpError extends Error {
  constructor({ status = 500, message = DEFAULT_MESSAGE } = {}) {
    super(message);
    this.status = status;
    this.message = message;
  }
}

module.exports = HttpError;
