const express = require('express');
const errorHandlerMiddleware = require('./middlewares/errors/error-handler.middleware');
const { initApi } = require('./api/api');
const { Router } = require('express');
const { ENV } = require('./common/enums/app/env.enum');
const jwt = require('jsonwebtoken');

const app = express();
app.use(express.json());

app.use(ENV.APP.API_PATH, initApi(Router));

app.use(errorHandlerMiddleware);
app.listen(ENV.APP.PORT, () => {
  console.log(`App listening at http://localhost:${ENV.APP.PORT}`);
});

console.log(jwt.sign({ type: 'admin' }, process.env.JWT_SECRET));
console.log(
  jwt.sign(
    { userId: '3da197de-5afd-4840-b359-5a150944bd7f' },
    process.env.JWT_SECRET
  )
);

// Do not change this line
module.exports = { app };
