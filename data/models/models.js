const joi = require('joi');
const { initUser } = require('./user.model');
const { initTransaction } = require('./transaction.model');
const { initEvent } = require('./event.model');
const { initBet } = require('./bet.model');

const User = initUser(joi);
const Bet = initBet(joi);
const Event = initEvent(joi);
const Transaction = initTransaction(joi);

module.exports = {
  User,
  Bet,
  Event,
  Transaction,
};
