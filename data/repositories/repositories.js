const { db } = require('../db/connection');

const BetRepository = require('./bet.repository');
const EventRepository = require('./event.repository');
const OddsRepository = require('./odds.repository');
const TransactionRepository = require('./transaction.repository');
const UserRepository = require('./user.repository');

const Bet = new BetRepository(db);
const Event = new EventRepository(db);
const Transaction = new TransactionRepository(db);
const Odds = new OddsRepository(db);
const User = new UserRepository(db);

module.exports = {
  Bet,
  Event,
  Odds,
  Transaction,
  User,
};
