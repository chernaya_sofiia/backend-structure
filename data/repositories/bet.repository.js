const BaseRepository = require('./base.repository');

class BetRepository extends BaseRepository {
  constructor(db) {
    super(db, 'bet');
  }
}

module.exports = BetRepository;
