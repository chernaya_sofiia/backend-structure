const BaseRepository = require('./base.repository');

class TransactionRepository extends BaseRepository {
  constructor(db) {
    super(db, 'transaction');
  }
}

module.exports = TransactionRepository;
