const BaseRepository = require('./base.repository');

class OddsRepository extends BaseRepository {
  constructor(db) {
    super(db, 'odds');
  }
}

module.exports = OddsRepository;
