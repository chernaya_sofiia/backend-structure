const BaseRepository = require('./base.repository');

class EventRepository extends BaseRepository {
  constructor(db) {
    super(db, 'event');
  }
}

module.exports = EventRepository;
