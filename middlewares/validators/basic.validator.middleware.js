const { HttpError } = require('../../exceptions/exceptions');

const BasicValidationResult = (
    validationSchema,
    data,
    next
) => {
    const isValidResult = validationSchema.validate(data);
    if (isValidResult.error) {
        throw new HttpError({
            status: 400,
            message: isValidResult.error.details[0].message,
        });
    } else {
        next();
    }
};

module.exports = {
    BasicValidationResult,
};