const joi = require('joi');
const { BasicValidationResult } = require('./basic.validator.middleware');

const postTransactionValidator = (req, res, next) => {
  const validationSchema = joi
    .object({
      id: joi.string().uuid(),
      userId: joi.string().uuid().required(),
      cardNumber: joi.string().required(),
      amount: joi.number().min(0).required(),
    })
    .required();
  BasicValidationResult(validationSchema, req.body, next);
};

module.exports = {
  postTransactionValidator,
};
