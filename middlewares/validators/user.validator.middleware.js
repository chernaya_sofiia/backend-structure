const joi = require('joi');
const { HttpError } = require('../../exceptions/exceptions');

const DefaultValidationResult = (
  schema,
  params,
  next,
  balance = false
) => {
  const isValidResult = schema.validate(params);
  if (isValidResult.error) {
    throw new HttpError({
      status: 400,
      message: isValidResult.error.details[0].message,
    });
  } else {
    if (balance) {
      params.balance = 0;
    }
    next();
  }
};

const getUserValidator = (req, res, next) => {
  const getUserSchema = joi
    .object({
      id: joi.string().uuid(),
    })
    .required();
    DefaultValidationResult(getUserSchema, req.params, next);
};

const postUserValidator = (req, res, next) => {
  const postUserSchema = joi
    .object({
      id: joi.string().uuid(),
      type: joi.string().required(),
      email: joi.string().email().required(),
      phone: joi
        .string()
        .pattern(/^\+?3?8?(0\d{9})$/)
        .required(),
      name: joi.string().required(),
      city: joi.string(),
    })
    .required();
    DefaultValidationResult(postUserSchema, req.body, next, true);
};

const putUserValidator = (req, res, next) => {
  const putUserSchema = joi
    .object({
      email: joi.string().email(),
      phone: joi.string().pattern(/^\+?3?8?(0\d{9})$/),
      name: joi.string(),
      city: joi.string(),
    })
    .required();
    DefaultValidationResult(putUserSchema, req.body, next);
};

module.exports = {
  getUserValidator,
  postUserValidator,
  putUserValidator,
};
