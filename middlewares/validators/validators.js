const {
  getUserValidator,
  postUserValidator,
  putUserValidator,
} = require('./user.validator.middleware');
const {
  postTransactionValidator,
} = require('./transaction.validator.middleware');
const {
  postEventValidator
} = require('./event.validator.middleware');

module.exports = {
  userValidator: {
    getUserValidator,
    postUserValidator,
    putUserValidator,
  },
  transactionValidator: {
    postTransactionValidator,
  },
  eventValidator: {
    postEventValidator,
  }
};
